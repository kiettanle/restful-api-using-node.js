const mongoose = require('mongoose');
const uuid = require('../helpers/uuid')
// Value of Status
const ACTIVE = 'Active';
const INACTIVE = 'Inactive';

const STATUS = [
    ACTIVE,
    INACTIVE,
];

const ADMIN = 'Admin';
const USER = 'User';

const ROLE = [
    ADMIN,
    USER,
];

const UserSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: { type: String, required: true },
    email: { type: String, required: true, trim: true, lowercase: true, unique: true, index: true, match: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ },
    password: { type: String, required: true },
    loginFailCount: { type: Number, required: false, default: 0 },
    nextLoginTimeIfMaxAttemptReached: { type: Date, required: false, default: null },
    lastLoginDate: { type: Date, required: false, default: null },
    maxAttempt: { type: Number, required: false, default: 5 },
    token: { type: String, required: true, default: uuid.uuid4 },
    tokenExpiredDate: { type: Date, required: false, default: new Date(+new Date() + 2 * 60000) },
    avatar: { type: String, default: "uploads/images/avatars/default.jpg" },
    orders: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Order' }],
    role: {
        type: String,
        required: false,
        enum: ROLE,
        default: USER
    },
    status: {
        type: String,
        required: false,
        enum: STATUS,
        default: INACTIVE
    },
    deleted_at: { type: Date, default: null }
},
    { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

const User = mongoose.model('User', UserSchema);

module.exports = User;