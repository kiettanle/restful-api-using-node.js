const mongoose = require('mongoose');

const ProductSchema = mongoose.Schema({
    _id: mongoose.Types.ObjectId,
    name: { type: String, required: true, trim: true,  },
    image: { type: String, default: 'uploads/images/products/default.jpg'},
    price: { type: Number, required: true , default : 0},
    deleted_at: {type: Date, default: null }
},
{ timestamps: { createdAt: 'created_at', updatedAt : 'updated_at' } });

module.exports = mongoose.model('Product', ProductSchema);