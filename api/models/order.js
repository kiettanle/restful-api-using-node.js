const mongoose = require('mongoose');

const OrderSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    user: { type: mongoose.Schema.Types.ObjectId, required:true, ref: 'User'},
    total: { type: Number, required: true , default : 0},
    status: { type: Number, required: true , default : 0},
    products : [{ type: mongoose.Schema.Types.ObjectId, ref: 'Product' }],
    deleted_at: {type: Date, default: null }
},
{ timestamps: { createdAt: 'created_at', updatedAt : 'updated_at' } });

module.exports = mongoose.model('Order', OrderSchema);