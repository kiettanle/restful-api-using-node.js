const nodemailer = require('nodemailer');
let account = {
    stmp: {
        host: process.env.SMTP_HOST,
        port: process.env.SMTP_PORT,
        secure: true

    },
    auth: {
        user: process.env.SMTP_USER,
        pass: process.env.SMTP_PASSWORD,
    }
}

function sendEmail(from, to, subject, text, htmlString) {
    // Generate SMTP service account from ethereal.email
    nodemailer.createTestAccount((err, account) => {
        if (err) {
            console.error('Failed to create a testing account. ' + err.message);
            return process.exit(1);
        }

        console.log('Credentials obtained, sending message...');

        // Create a SMTP transporter object
        let transporter = nodemailer.createTransport({
            host: account.smtp.host,
            port: account.smtp.port,
            secure: account.smtp.secure,
            auth: {
                user: account.user,
                pass: account.pass
            }
        });

        // Message object
        let message = {
            from: from,
            to: to,
            subject: subject,
            text: text,
            html: htmlString,
        };

        transporter.sendMail(message, (err, info) => {
            if (err) {
                console.log('Error occurred. ' + err.message);
                return process.exit(1);
            }

            console.log('Message sent: %s', info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
        });
    });
}
function testSendMail() {
    console.log('Sent...');
}
module.exports = {
    sendEmail,
    testSendMail,
}
// sendEmail(process.env.SMTP_FROM, 'kiet@gmial.com', 'Verify your account', 'Click on Verify button to verify account', '<h1>Active your account</h1>')