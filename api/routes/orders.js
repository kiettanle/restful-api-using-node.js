const express = require('express');
const router = express.Router();
const OrderController = require('../controllers/orders');
const AuthChecker = require('../middlewares/auth')

router.get('/', AuthChecker, OrderController.all);

router.post('/', AuthChecker, OrderController.createOrder);

router.get('/:id', AuthChecker, OrderController.get);

router.delete('/:id', AuthChecker, OrderController.deleteOrder);

module.exports = router;