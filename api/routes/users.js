const express = require('express');
const router = express.Router();
const UserController = require('../controllers/users');

router.get('/', UserController.all);
router.post('/', UserController.create);
router.get('/:id', UserController.get);
// // router.patch('/:id', UserController.update);
router.delete('/:id', UserController.delete);
router.post('/login', UserController.login);
router.get('/verify/:token', UserController.verifyAccount);
router.post('/verify', UserController.verify);
router.post('/password/forgot', UserController.forgotPassword);
router.post('/password/reset', UserController.resetPassword);

module.exports = router;