const express = require('express');
const router = express.Router();
const ProductController = require('../controllers/products');
const Auth = require('../middlewares/auth');

const multer = require('multer');
const fileFilter = (req, file, cb) => {
    if(file.mimetype === 'image/jpg' || file.mimetype === 'image/png' || file.mimetype === 'image/gif')
    {
        cb(null, true);
    }
    else {
        return cb(null, false);
    }
};

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/images/products/');
    },
    filename: function(req, file, cb) {
        cb(null, Date.now() + file.originalname.substring(file.originalname.length - 4, file.originalname.length));
    }
});
const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
});

router.get('/', ProductController.all);
router.post('/',  upload.single('image'), Auth, ProductController.create);
router.get('/:id', ProductController.get);
router.patch('/:id', ProductController.update);
router.delete('/:id', ProductController.delete);

module.exports = router;