const responser = require('../helpers/response');
const User = require('../models/user');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const uuid = require('../helpers/uuid');
const emailServices = require('../helpers/email');

/**
 * Get all user
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function all(req, res, next) {
    const users = await User.find().populate(
        {
            path: 'orders',
            select: '_id total status products user created_at updated_at',
            populate: {
                path: 'products user',
                select: '_id name image',
            }
        });
    if (users.length > 0) {
        const data = await {
            count: users.length,
            users: users.map(user => {
                return {
                    _id: user._id,
                    name: user.name,
                    email: user.email,
                    orders: user.orders,
                    status: user.status,
                    role: user.role,
                    avatar: user.avatar,
                    token: user.token,
                    password: user.password,
                    tokenExpiredDate: user.tokenExpiredDate,
                    created_at: user.created_at,
                    updated_at: user.updated_at,
                    deleted_at: user.deleted_at
                }
            })
        };
        responser.response(res, 200, "All users", data);
    }
    else {
        responser.response(res, 200, "No users", null);
    }
}
/**
 * Get User By Id
 * 
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function get(req, res, next) {
    const user = await User.findById({ _id: req.params.id })
        .populate(
            {
                path: 'orders',
                select: '_id total status products user created_at updated_at',
                populate: {
                    path: 'products user',
                    select: '_id name image',
                }
            });
    try {
        if (user) {
            const data = {
                _id: user._id,
                name: user.name,
                email: user.email,
                status: user.status,
                role: user.role,
                orders: user.orders,
                avatar: user.avatar,
                created_at: user.created_at,
                updated_at: user.updated_at,
                deleted_at: user.deleted_at,
                tokenExpiredDate: user.tokenExpiredDate,
                token: user.token,
                loginFailCount: user.loginFailCount,
                lastLoginDate: user.lastLoginDate,
                nextLoginTimeIfMaxAttemptReached: user.nextLoginTimeIfMaxAttemptReached
            }
            responser.response(res, 200, "Get user successfully", data);
        }
        else
            responser.response(res, 200, "User not found", null);
    }
    catch (err) {
        responser.response(res, 500, err.message, err);
    }
}

//Create New User
async function create(req, res, next) {
    try {
        var existingUser = await User.findOne({ email: req.body.email });
        if (existingUser) {
            responser.response(res, 409, "Email existed", null);
        }
        else {
            const user = new User({
                _id: new mongoose.Types.ObjectId(),
                name: req.body.name,
                email: req.body.email,
                role: req.body.role,
                password: bcrypt.hashSync(req.body.password, 10)
            });
            //Send verification email
            await emailServices.sendEmail(process.env.SMTP_FROM, user.email, 'Verify your account', 'Click on Verify button to verify account', `<h1><a href ="${process.env.BASE_URL}/users/verify/${user.token}">Verify Account</a></h1>`);
            user.save();
            console.log(user);
            responser.response(res, 200, "User has been created successfully", user);
        }
    }
    catch (err) {
        responser.response(res, 500, err, null);
    }
}

/**
 *Delete user
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function deleteUser(req, res, next) {
    try {
        var user = await User.findByIdAndDelete({ _id: req.params.id });
        console.log(user)
        if (user)
            responser.response(res, 200, "User has been deleted successfully", null);
        else
            responser.response(res, 404, "User Not Found", null);
    }
    catch (err) {
        responser.response(res, 500, err, null);
    }
}

/**
 * Login
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function login(req, res, next) {
    try {
        var user = await User.findOne({ email: req.body.email });
        if (!user)
            responser.response(res, 401, "Email does not exist", null);
        else {
            if (user.nextLoginTimeIfMaxAttemptReached > new Date())
                return responser.response(res, 401, `Your account has been disabled for ${(Math.abs(user.nextLoginTimeIfMaxAttemptReached.getTime() - new Date().getTime()) / 1000)} seconds`, null);
            else {
                var result = await bcrypt.compareSync(req.body.password, user.password);
                if (result) {
                    const token = await jwt.sign({
                        userId: user._id,
                        email: user.email
                    },
                        "key",
                        {
                            expiresIn: "24h"
                        }
                    );
                    user.lastLoginDate = new Date();
                    user.loginFailCount = 0;
                    user.nextLoginTimeIfMaxAttemptReached = null;
                    user.save();
                    return responser.response(res, 200, "User has been logged in successfully", { user, token: token });
                }
                else {
                    if (user.nextLoginTimeIfMaxAttemptReached > new Date()) {

                    }
                    if (user.loginFailCount >= user.maxAttempt) {
                        user.nextLoginTimeIfMaxAttemptReached = new Date(+new Date() + 1 * 60000);
                        user.save();
                        return responser.response(res, 401, "Your account has been disabled!", null);
                    }
                    else {
                        user.loginFailCount += 1;
                        user.save();
                        return responser.response(res, 401, "Email or password is incorrect", null);
                    }
                }
            }
        }
    }
    catch (err) {
        responser.response(res, 500, err, null);
    }
}

async function verifyAccount(req, res, next) {
    try {
        var user = await User.findOne({ token: req.params.token });
        if (user) {
            if (user.tokenExpiredDate < new Date()) {
                responser.response(res, 404, "Token expired", user);
            }
            user.status = 'Active';
            user.save();
            responser.response(res, 200, "Your account has been verified successfully!", user);
        }
        else {
            responser.response(res, 404, "Token does not exist", user);
        }

    }
    catch (err) {
        responser.response(res, 500, "Your account has not been verified successfully!", err);
    }
}

async function verifyUser(req, res, next) {
    try {
        var user = await User.findOne({ token: req.body.token });
        if (user) {
            if (user.tokenExpiredDate < new Date()) {
                responser.response(res, 404, "Token expired", user);
            }
            user.status = 'Active';
            user.save();
            responser.response(res, 200, "Your account has been verified successfully!", user);
        }
        else {
            responser.response(res, 404, "Token does not exist", user);
        }

    }
    catch (err) {
        responser.response(res, 500, "Your account has not been verified successfully!", err);
    }
}
/**
 * Forgot password
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function forgotPassword(req, res, next) {
    try {
        var user = await User.findOne({ email: req.body.email });
        console.log(user)
        if (user) {
            user.token = uuid.uuid4();
            user.tokenExpiredDate = await new Date(+new Date() + 2 * 60000);
            user.save();
            responser.response(res, 200, `An reset password email has been send to your email ${user.email}!`, user);
        }
        else {
            responser.response(res, 404, "Account does not exist or deleted!", user);
        }
    }
    catch (err) {
        responser.response(res, 500, "Can not reset password!", err);
    }
}
/**
 * Reset user password
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function resetPassword(req, res, next) {
    try {
        console.log(req.body.token)
        var user = await User.findOne({ token: req.body.token });
        if (user) {
            console.log(user)
            if (user.tokenExpiredDate < new Date()) {
                responser.response(res, 404, "Token does not exist or expired", null);
            }
            if (req.body.newPassword !== req.body.newPasswordConfirm)
                responser.response(res, 404, "New password mismatch!", null);
            user.password = await bcrypt.hashSync(req.body.newPassword, 10);
            await user.save();
            responser.response(res, 200, "Your password has been reset successfully!", user);
        }
        else {
            responser.response(res, 404, "Account does not exist", null);
        }
    }
    catch (err) {
        responser.response(res, 500, "Your account has not been verified successfully!", err);
    }
}

module.exports = {
    create,
    verify: verifyUser,
    login,
    all,
    get,
    delete: deleteUser,
    forgotPassword,
    resetPassword,
    verifyAccount
}