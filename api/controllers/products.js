const responser = require('../helpers/response');
const Product = require('../models/product');
const mongoose = require('mongoose');



//Get all products
exports.all = (req, res, next) => {
    const products = Product.find().exec()
        .then(result => {
            if (result.length > 0) {
                const data = {
                    count: result.length,
                    products: result.map(result => {
                        return {
                            _id: result._id,
                            name: result.name,
                            price: result.price,
                            image: result.image,
                            created_at: result.created_at,
                            updated_at: result.updated_at,
                            deleted_at: result.deleted_at
                        }
                    })
                };
                responser.response(res, 200, "All products", data);
            }
            else
                responser.response(res, 200, "No products", null);
        })
        .catch(err => {
            responser.response(res, 500, err, null);
        }
    );
}

//Get Product By Id
exports.get = (req, res, next) => {
    Product.findById(req.params.id).exec()
        .then(result => {
            if (result) {
                const data  = {
                    _id: result._id,
                    name: result.name,
                    price: result.price,
                    created_at: result.created_at,
                    updated_at: result.updated_at,
                    deleted_at: result.deleted_at
                };
                responser.response(res, 200, "Get Product", data);
            }
            else
                responser.response(res, 404, "Product Not Found!", null);
        })
        .catch(err => {
            responser.response(res, 500, err, null);
        });
}

//Create New Product
exports.create = (req, res, next) => {
    const product = new Product({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        price: req.body.price
    });

    product.save()
        .then(result => {
            responser.response(res, 200, "Product has been created successfully", result);
        })
        .catch(err => {
            responser.response(res, 500, err, req.body.token);
        }
    );
}

//Update Existing Product
exports.update = (req, res, next) => {
    Product.update({ _id: req.params.id }, { $set: { name: req.body.name, price: req.body.price } }).exec()
        .then(result => {
            if (result.n) {
                const data  = {
                    _id: result._id,
                    name: result.name,
                    price: result.price
                };
                responser.response(res, 200, "Product has been updated successfully", data);
            }
            else
                responser.response(res, 404, "Product Not Found", null);
        })
        .catch(err => {
            responser.response(res, 500, err, null);
        }
    );
}

//Delete Product
exports.delete = (req, res, next) => {
    Product.remove({ _id: req.params.id }).exec()
        .then(result => {
            if (result.n)
                responser.response(res, 200, "Product has been deleted successfully", null);
            else
                responser.response(res, 404, "Product Not Found", null);
        })
        .catch(err => {
            responser.response(res, 500, err, null);
        }
    );
}