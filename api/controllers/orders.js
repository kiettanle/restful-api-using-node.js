const responser = require('../helpers/response');
const Order = require('../models/order');
const User = require('../models/user');
const Product = require('../models/product');
const mongoose = require('mongoose')
//Get all Orders
function all(req, res, next) {
    const orders = Order.find().populate('user', '_id name').populate('products', '_id name image').exec()
        .then(result => {
            if (result.length > 0) {
                const data = {
                    count: result.length,
                    orders: result.map(result => {
                        return {
                            _id: result._id,
                            userId: result.userId,
                            user: result.user,
                            products: result.products,
                            total: result.total,
                            created_at: result.created_at,
                            updated_at: result.updated_at,
                            deleted_at: result.deleted_at
                        }
                    })
                };
                responser.response(res, 200, "All Orders", data);
            }
            else
                responser.response(res, 200, "No Orders", null);
        })
        .catch(err => {
            responser.response(res, 500, err, null);
        }
        );
}

//Get Order By Id
function get(req, res, next) {
    Order.findById(req.params.id).populate('user', '_id name').populate('products', '_id name image').exec()
        .then(result => {
            if (result) {
                const data = {
                    _id: result._id,
                    user: result.user,
                    products: result.products,
                    total: result.total,
                    status: result.status,
                    created_at: result.created_at,
                    updated_at: result.updated_at,
                    deleted_at: result.deleted_at
                };
                responser.response(res, 200, "Get Order", data);
            }
            else
                responser.response(res, 404, "Order Not Found!", null);
        })
        .catch(err => {
            responser.response(res, 500, err, null);
        });
}

//Create New Order
function create(req, res, next) {

    const order = new Order({
        _id: new mongoose.Types.ObjectId(),
        total: req.body.total,
        user: req.body.user
    });
    order.save()
        .then(result => {
            const user = User.findByIdAndUpdate(
                { _id: req.body.user },
                { $push: { orders: order } },
                function (err, model) {
                    console.log(err);
                }).exec()
                .then()
                .catch(err => {
                    responser.response(res, 500, err, null);
                });
            responser.response(res, 200, "Order has been created successfully", result);
        })
        .catch(err => {
            responser.response(res, 500, err, null);
        });
}
async function createOrder(req, res, next) {
    try {
        const order = new Order({
            _id: new mongoose.Types.ObjectId(),
            total: req.body.total,
            user: req.body.user
        });
        let productIdArray = await req.body.products.map(product => new mongoose.Types.ObjectId(product.id));

        var products = await Product.find({
            '_id': {
                $in: productIdArray
            }
        }).select('_id name');

        let createdOrder = await Order.create(order);

        await Order.findByIdAndUpdate({ _id: createdOrder._id }, { $push: { products: products } });

        await User.findByIdAndUpdate({ _id: req.body.user }, { $push: { orders: order } });

        responser.response(res, 200, "Order has been created successfully", order);
    }
    catch (err) {
        responser.response(res, 500, err, null);
    }
}

//Update Existing Order
function update(req, res, next) {
    Order.update({ _id: req.params.id }, { $set: { name: req.body.name, price: req.body.price } }).exec()
        .then(result => {
            if (result.n) {
                const data = {
                    _id: result._id,
                    name: result.name,
                    price: result.price
                };
                responser.response(res, 200, "Order has been updated successfully", data);
            }
            else
                responser.response(res, 404, "Order Not Found", null);
        })
        .catch(err => {
            responser.response(res, 500, err, null);
        }
        );
}

//Delete Order
function deleteOrder(req, res, next) {
    Order.remove({ _id: req.params.id }).exec()
        .then(result => {
            if (result.n)
                responser.response(res, 200, "Order has been deleted successfully", null);
            else
                responser.response(res, 404, "Order Not Found", null);
        })
        .catch(err => {
            responser.response(res, 500, err, null);
        }
        );
}
module.exports = {
    update, create, all, get, deleteOrder, createOrder
}